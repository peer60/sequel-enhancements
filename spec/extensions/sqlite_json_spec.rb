require 'spec_helper'
require 'json'

describe 'sqlite_json extension' do
  let(:db){ Sequel.connect("sqlite:///") }
  let(:model_class){ Class.new(Sequel::Model(db[:people])){ @plugins.clear } }

  subject { model_class.new }

  before do
    db.extension :sqlite_json
    db.create_table(:people) do
      primary_key :id
      column :name, :text
      column :address, :json
      column :phones, 'text[]'
      column :lucky_numbers, 'int[]'
    end
  end

  it 'stores and retrieves json object' do
    original = {'street' => '123 fake'}
    subject.name = 'foo'
    subject.address = original
    subject.save
    subject.address.must_equal original
  end

  it 'stores and retrieves arrays of strings' do
    original = ['18008675309', '18005555555']
    subject.phones = original
    subject.save
    subject.phones.must_equal original
  end

  it 'stores and retrieves arrays of ints' do
    original = ['1','2','3']
    subject.lucky_numbers = original
    subject.save
    subject.lucky_numbers.must_equal original
  end
end
