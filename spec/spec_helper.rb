require "bundler/setup"
require "sequel"
require "minitest/autorun"

class << Sequel::Model
  attr_writer :db_schema
  alias orig_columns columns
  def columns(*cols)
    return super if cols.empty?
    define_method(:columns){cols}
    @dataset.instance_variable_set(:@columns, cols) if @dataset
    def_column_accessor(*cols)
    @columns = cols
    @db_schema = {}
    cols.each{|c| @db_schema[c] = {}}
  end
end

class Minitest::Spec
  FIXNUM_MAX = 2**(0.size * 8 - 2)

  def rand_s
    SecureRandom.hex
  end

  def rand_i(max=FIXNUM_MAX)
    SecureRandom.random_number(max)
  end
end

class CustomFilter
  def self.filter(bt)
    return ['No backtrace'] unless bt

    new_bt = bt.take_while { |line| line !~ %r{minitest} }
    new_bt = bt.select     { |line| line !~ %r{minitest} } if new_bt.empty?
    new_bt = bt.dup if new_bt.empty?

    new_bt
  end
end

Minitest.backtrace_filter = CustomFilter
