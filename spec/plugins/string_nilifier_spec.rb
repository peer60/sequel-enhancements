require 'spec_helper'

describe 'Sequel::Plugins::StringNilifier' do
  let(:db){ Sequel.mock }
  let(:model_class){ Class.new(Sequel::Model(db[:test])){ @plugins.clear } }

  subject { model_class.new }

  before do
    model_class.columns :name, :b
    model_class.db_schema[:b][:type] = :blob
    model_class.plugin :string_nilifier
  end

  it 'nilifies empty strings' do
    subject.name = ''
    subject.name.must_be_nil
  end

  it 'doesnt touch other fields' do
    subject.name = 'hey'
    subject.name.must_equal 'hey'

    subject.name = 6
    subject.name.must_equal 6
  end

  it 'doesnt affect blob arguments' do
    expected = Sequel.blob('')
    subject.name = expected
    subject.name.must_equal expected
  end

  it 'doesnt affect blob columns' do
    model_class.must_be :skip_string_nilifying?, :b
    subject.b = ''
    subject.b.must_be_instance_of Sequel::SQL::Blob
    subject.b.must_equal Sequel.blob('')
  end

  it 'allows skipping columns with Model.skip_string_nilifying' do
    model_class.skip_string_nilifying :name
    model_class.must_be :skip_string_nilifying?, :name
    subject.name = ''
    subject.name.must_equal ''
  end

end
