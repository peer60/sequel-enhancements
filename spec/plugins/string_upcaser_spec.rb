require 'spec_helper'

describe 'Sequel::Plugins::StringUpcaser' do
  let(:db){ Sequel.mock }
  let(:model_class){ Class.new(Sequel::Model(db[:test])){ @plugins.clear } }

  subject { model_class.new }

  before do
    model_class.columns :name, :other
    model_class.plugin :string_upcaser, :name
  end

  it 'upcases specified column' do
    expected = rand_s.upcase
    subject.name = expected.downcase
    subject.name.must_equal expected
  end

  it 'doesnt affect unspecified columns' do
    expected = rand_s.downcase
    subject.other = expected
    subject.other.must_equal expected
  end

  it 'handles nil value' do
    subject.name = nil
    subject.name.must_be_nil
  end

  it 'handles non-string value' do
    subject.name = 7
    subject.name.must_equal 7
  end

  it 'leaves blob alone' do
    expected = Sequel.blob rand_s.downcase
    subject.name = expected
    subject.name.must_be_instance_of Sequel::SQL::Blob
    subject.name.must_equal expected
  end
end
