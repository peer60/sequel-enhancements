require 'spec_helper'

describe 'Sequel::Plugins::StringUpcaser' do
  let(:db){ Sequel.mock }
  let(:model_class){ Class.new(Sequel::Model(db[:test])){ @plugins.clear } }

  subject { model_class.new }

  before do
    model_class.columns :id, :other_uuid
    model_class.plugin :skip_create_refresh
    model_class.plugin :uuid, [:id, :other_uuid]
  end

  it 'sets uuid after save' do
    subject.id.must_be_nil
    subject.other_uuid.must_be_nil
    subject.save
    subject.id.wont_be_nil
    subject.other_uuid.wont_be_nil
    subject.other_uuid.wont_equal subject.id
  end

  it 'sets uuid before validation' do
    subject.id.must_be_nil
    subject.valid?
    subject.id.wont_be_nil
  end

  describe 'prefix' do
    let(:prefix){ SecureRandom.hex(1) }

    before do
      Sequel::Plugins::Uuid.instance_eval{Hash(@model_prefixes).clear}
      model_class.columns :id, :other_uuid
      model_class.instance_eval { @plugins.clear }
      model_class.plugin :uuid, :id, :prefix => prefix
    end

    it 'sets prefix on uuids' do
      subject.valid?
      subject.id[0..1].must_equal prefix
    end

    it 'allows looking up a model by prefix' do
      Sequel::Plugins::Uuid.model_for_prefix(prefix).must_equal model_class
    end

    it 'explodes when two models have the same prefix' do
      other_model = Class.new(Sequel::Model(db[:test])){ @plugins.clear }
      other_model.columns :id
      other_model.instance_eval { @plugins.clear }
      lambda do
        other_model.plugin :uuid, :id, :prefix => prefix
      end.must_raise Sequel::Plugins::Uuid::ExistingModelPrefixError
    end
  end
end
