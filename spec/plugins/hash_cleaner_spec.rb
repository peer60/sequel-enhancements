require 'spec_helper'

describe 'Sequel::Plugins::HashCleaner' do
  let(:db){ Sequel.mock }
  let(:model_class){ Class.new(Sequel::Model(db[:test])){ @plugins.clear } }

  subject { model_class.new }

  before do
    model_class.columns :json_field, :other
    model_class.plugin :hash_cleaner
  end

  after do
    model_class.instance_variable_set :'@plugins', @previous_plugins
  end

  it 'clears nil' do
    subject.json_field = {'value' => nil}
    subject.json_field.must_equal({})
  end

  it 'clears empty word' do
    subject.json_field = {'value' => ' '}
    subject.json_field.must_equal({})
  end

  it 'cleans word' do
    expected = 'something'
    subject.json_field = {'value' => "\xC2\xA0 #{expected}\xC2\xA0 "}
    subject.json_field.must_equal 'value' => expected
  end

  it 'doesnt touch inner characters' do
    expected = "some \t \xC2\xA0 \n thing"
    subject.json_field = {'value' => "\xC2\xA0 #{expected}\xC2\xA0 "}
    subject.json_field.must_equal 'value' => expected
  end

  it 'doesnt affect falsy values' do
    expected = 0
    subject.json_field = {'value' => expected}
    subject.json_field.must_equal 'value' => expected

    expected = false
    subject.json_field = {'value' => expected}
    subject.json_field.must_equal 'value' => expected

    expected = []
    subject.json_field = {'value' => expected}
    subject.json_field.must_equal 'value' => expected
  end

  it 'doesnt affect non-hash value' do
    expected = " can't touch this "
    subject.json_field = expected
    subject.json_field.must_equal expected
  end
end
