require 'spec_helper'

describe 'Sequel::Plugins::StringCleaner' do
  let(:db){ Sequel.mock }
  let(:model_class){ Class.new(Sequel::Model(db[:test])){ @plugins.clear } }

  subject { model_class.new }

  before do
    model_class.columns :name, :other
    model_class.plugin :string_cleaner
  end

  it 'cleans word' do
    expected = 'something'
    subject.name = "\xC2\xA0 #{expected}\xC2\xA0 "
    subject.name.must_equal expected
  end

  it 'doesnt touch inner characters' do
    expected = "some \t \xC2\xA0 \n thing"
    subject.name = "\xC2\xA0 #{expected}\xC2\xA0 "
    subject.name.must_equal expected
  end

  it 'doesnt affect non-string inputs' do
    expected = rand_i
    subject.name = expected
    subject.name.must_equal expected
  end

  it 'doesnt blob alone' do
    expected = Sequel.blob " #{rand_s} "
    subject.name = expected
    subject.name.must_be_instance_of Sequel::SQL::Blob
    subject.name.must_equal expected
  end
end
