require 'spec_helper'

describe 'Sequel::Plugins::StringDowncaser' do
  let(:db){ Sequel.mock }
  let(:model_class){ Class.new(Sequel::Model(db[:test])){ @plugins.clear } }

  subject { model_class.new }

  before do
    model_class.columns :name, :other
    model_class.plugin :string_downcaser, :name
  end

  it 'downcases specified column' do
    expected = rand_s
    subject.name = expected.upcase
    subject.name.must_equal expected
  end

  it 'doesnt affect unspecified columns' do
    expected = rand_s.upcase
    subject.other = expected
    subject.other.must_equal expected
  end

  it 'handles nil value' do
    subject.name = nil
    subject.name.must_be_nil
  end

  it 'handles non-string value' do
    subject.name = 7
    subject.name.must_equal 7
  end

  it 'leaves blob alone' do
    expected = Sequel.blob rand_s.upcase
    subject.name = expected
    subject.name.must_be_instance_of Sequel::SQL::Blob
    subject.name.must_equal expected
  end
end
