# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'sequel/enhancements/version'

Gem::Specification.new do |spec|
  spec.name          = "sequel-enhancements"
  spec.version       = Sequel::Enhancements::VERSION
  spec.authors       = ["peer60"]
  spec.email         = ["development@peer60.com"]
  spec.summary       = %q{Plugins and extensions for Sequel}
  spec.description   = %q{Plugins and extensions for Sequel}
  spec.homepage      = "https://bitbucket.org/peer60/sequel-enhancements"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]


  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.4"
  spec.add_development_dependency "sequel"
  spec.add_development_dependency "sqlite3"
end
