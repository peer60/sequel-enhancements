# Sequel::Enhancements

peer60 plugins and extensions for [Sequel](http://sequel.jeremyevans.net/)

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'sequel-enhancements'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install sequel-enhancements

## Usage

Require plugins and extensions as normal

```ruby
Sequel::Model.plugin :string_nilifier
```

## Included plugins

- `hash_cleaner` strips trailing and leading invisibles from 
- `string_cleaner` a stronger version of `string_stripper`, removing all invisibles
- `string_downcaser` force downcasing of specified columns
- `string_nilifier` sets empty strings to nil
- `string_upcaser` force upcasing of specified columns

## Included extensions

- `sqlite_json` json support for SQLite


## Contributing

1. Fork it ( https://github.com/[my-github-username]/sequel-enhancements/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
