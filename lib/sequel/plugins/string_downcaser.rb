require "sequel/enhancements"

module Sequel
  module Plugins
    module StringDowncaser
      def self.apply(model, *args)
        model.instance_eval do
          @downcase_columns = Set.new
        end
      end

      def self.configure(model, column)
        model.add_downcase_column column
      end

      module ClassMethods
        Plugins.inherited_instance_variables(self, :@downcase_columns => :dup)

        def add_downcase_column(column)
          @downcase_columns << column
        end

        def downcase_column?(column)
          @downcase_columns.include? column
        end
      end

      module InstanceMethods
        def []=(k, v)
          if model.downcase_column?(k) && v.is_a?(String) && !v.is_a?(SQL::Blob)
            v = v.downcase
          end
          super
        end
      end
    end
  end
end
