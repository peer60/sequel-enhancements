require "sequel/enhancements"

module Sequel
  module Plugins
    module StringNilifier
      def self.apply(model)
        model.plugin(:input_transformer, :string_nilifier) do |v|
          (v.is_a?(String) && !v.is_a?(SQL::Blob) && v.empty?) ? nil : v
        end
      end

      def self.configure(model)
        model.instance_eval{set_skipped_string_nilifying_columns if @dataset}
      end

      module ClassMethods
        Plugins.after_set_dataset(self, :set_skipped_string_nilifying_columns)

        def skip_string_nilifying(*columns)
          skip_input_transformer(:string_nilifier, *columns)
        end

        def skip_string_nilifying?(column)
          skip_input_transformer?(:string_nilifier, column)
        end

        private

        def set_skipped_string_nilifying_columns
          if @db_schema
            blob_columns = @db_schema.map{|k,v| k if v[:type] == :blob}.compact
            skip_string_nilifying(*blob_columns)
          end
        end
      end
    end
  end
end
