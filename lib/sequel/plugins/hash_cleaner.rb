require "sequel/enhancements"

module Sequel
  module Plugins
    module HashCleaner
      SURROUNDING_INVISIBLES = /\A[^[:graph:]]+|[^[:graph:]]+\z/.freeze

      def self.apply(model, *args)
        model.plugin(:input_transformer, :hash_cleaner){ |v| v.is_a?(Hash) ? clean(v) : v }
      end

      private

      def self.clean(value)
        if value.is_a?(Hash)
          value.each_with_object({}) do |(k, v), memo|
            cleaned = clean v
            memo[k] = cleaned unless exclude?(cleaned)
          end
        else
          Sequel::Enhancements.trim_invisible(value)
        end
      end

      def self.clean_value(value)
        value.is_a?(String) ? value.trim_invisible : value
      end

      def self.exclude?(value)
        value.nil? || (value.is_a?(String) && value.empty?)
      end
    end
  end
end
