require "sequel/enhancements"

module Sequel
  module Plugins
    module StringUpcaser
      def self.apply(model, *args)
        model.instance_eval do
          @upcase_columns = Set.new
        end
      end

      def self.configure(model, column)
        model.add_upcase_column column
      end

      module ClassMethods
        Plugins.inherited_instance_variables(self, :@upcase_columns => :dup)

        def add_upcase_column(column)
          @upcase_columns << column
        end

        def upcase_column?(column)
          @upcase_columns.include? column
        end
      end

      module InstanceMethods
        def []=(k, v)
          if model.upcase_column?(k) && v.is_a?(String) && !v.is_a?(SQL::Blob)
            v = v.upcase
          end
          super
        end
      end
    end
  end
end
