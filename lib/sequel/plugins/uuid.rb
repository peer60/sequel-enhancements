require "sequel/enhancements"
require "set"
require "securerandom"

module Sequel
  module Plugins
    module Uuid
      class ExistingModelPrefixError < Sequel::Error; end

      def self.configure(model, columns, opts={})
        columns = Array(columns)
        set_model_prefix(opts[:prefix], model) if opts[:prefix]
        model.instance_eval do
          @uuid_prefix = opts[:prefix]
          @uuid_fields ||= Set.new
          columns.each do |c|
            @uuid_fields << c
          end
        end
      end

      def self.model_for_prefix(prefix)
        Hash(@model_prefixes)[prefix]
      end

      def self.set_model_prefix(prefix, model)
        existing = model_for_prefix prefix
        if existing
        raise ExistingModelPrefixError.new("The prefix '#{prefix}' is already in use by #{existing}")
        end
        @model_prefixes ||= {}
        @model_prefixes[prefix] = model
      end

      module ClassMethods
        attr_reader :uuid_fields
        attr_reader :uuid_prefix
      end

      module InstanceMethods
        def _before_validation
          set_uuids if new?
          super
        end

        def set_uuids
          model.uuid_fields.each do |field|
            meth = :"#{field}="
            send(meth, generate_uuid) if respond_to?(meth)
          end
        end

        def generate_uuid
          orig = SecureRandom.uuid
          if model.uuid_prefix
            model.uuid_prefix.chars.each_with_index do |c, i|
              orig[i] = c
            end
          end
          orig
        end
      end
    end
  end
end
