require "sequel/enhancements"

module Sequel
  module Plugins
    module StringCleaner
      def self.apply(model, *args)
        model.plugin(:input_transformer, :string_cleaner) do |v|
          Enhancements.trim_invisible(v)
        end
      end
    end
  end
end
