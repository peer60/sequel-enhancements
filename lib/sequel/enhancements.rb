require "sequel/enhancements/version"

module Sequel
  module Enhancements
    extend self
    SURROUNDING_INVISIBLES = /\A[^[:graph:]]+|[^[:graph:]]+\z/.freeze


    def trim_invisible(str)
      if str.is_a?(String) && !str.is_a?(SQL::Blob)
        str.gsub(SURROUNDING_INVISIBLES, '')
      else
        str
      end
    end
  end
end
