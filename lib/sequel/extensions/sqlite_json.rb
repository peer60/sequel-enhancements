module Sequel
  module SQLite
    class JSONArray < DelegateClass(Array)
      include Sequel::SQL::AliasMethods

      # Convert the array to a json string, append a
      # literalized version of the string to the sql, and explicitly
      # cast the string to json.
      def sql_literal_append(ds, sql)
        ds.literal_append(sql, Sequel.object_to_json(self))
      end
    end

    class JSONHash < DelegateClass(Hash)
      include Sequel::SQL::AliasMethods

      # Convert the hash to a json string, append a
      # literalized version of the string to the sql, and explicitly
      # cast the string to json.
      def sql_literal_append(ds, sql)
        ds.literal_append(sql, Sequel.object_to_json(self))
      end

      # Return the object being delegated to.
      alias to_hash __getobj__
    end

    module DatabaseMethods
      JSON_TYPES = %w{
        json varchar[] text[] integer[] int[] boolean[] money[]
      }.map(&:freeze).freeze

      def self.extended(db)
        procs = db.conversion_procs
        parse_method = method :db_parse_json
        JSON_TYPES.each do |type|
          procs[type] = parse_method
        end
      end

      def self.db_parse_json(s)
        parse_json(s)
      rescue Sequel.json_parser_error_class => e
        raise Sequel.convert_exception_class(e, Sequel::InvalidValue)
      end

      def self.parse_json(s)
        begin
          value = Sequel.parse_json(s)
        rescue Sequel.json_parser_error_class => e
          raise Sequel.convert_exception_class(e, Sequel::InvalidValue)
        end

        case value
        when Array
          JSONArray.new(value)
        when Hash
          JSONHash.new(value)
        else
          raise Sequel::InvalidValue, "unhandled json value: #{value.inspect} (from #{s.inspect})"
        end
      end

      def schema_column_type(db_type)
        case db_type
        when 'json'
          'json'
        when /\[\]$/
          'json'
        else
          super
        end
      end

      def typecast_value_json(value)
        case value
        when JSONArray, JSONHash
          value
        when Array
          JSONArray.new(value)
        when Hash
          JSONHash.new(value)
        when String
          DatabaseMethods.parse_json(value)
        else
          raise Sequel::InvalidValue, "invalid value for json: #{value.inspect}"
        end
      end
    end
  end

  Database.register_extension :sqlite_json, SQLite::DatabaseMethods
end
